FROM registry.gitlab.com/amer.hasanovic/fet_base:latest


RUN apt-get update && apt-get upgrade -y  && \ 
     DEBIAN_FRONTEND=noninteractive apt-get -y install qemu-user qemu-system-x86 gcc-multilib && \ 
     mkdir -p /opt/logisim

ADD cs3410.jar /opt/logisim/
ADD logisim-evolution.jar /opt/logisim/
ADD ellcc.tar.gz /opt/
ADD OpenJDK11.tar.gz /opt/

RUN cd /opt/ellcc && ./ellcc install && chmod -R 777 /opt/ellcc

ENV PATH /opt/ellcc/bin:/opt/jre/bin:$PATH

ADD script.sh /opt1/
 
